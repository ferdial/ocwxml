<?xml version="1.0" encoding="UTF-8"?>
<section identifier="someidentifier">
    <title>Syllabus </title>
    <location> https://ocw.mit.edu/courses/history/21h-343j-making-books-the-renaissance-and-today-spring-2016/syllabus </location>
    <description>
        <![CDATA[
            This syllabus section provides the course description and information on meeting times, prerequisites, grading policy, required readings, and printing press construction.
        ]]>
    </description>
    <short_url>syllabus</short_url>
    <alternate_urls>
        <url>
            <original></original>
            <replacement></replacement>
        </url>
        <url>
            <original></original>
            <replacement></replacement>
        </url>
    </alternate_urls>
    <other_platform_requirements>
    </other_platform_requirements>
    <!-- if empty, it's a first level section -->
    <parent_id></parent_id>
    <keywords></keywords>
    <nav_label>Syllabus</nav_label>
    <always_publish value="no"/>
    <is_image_gallery value="no"/>
    <is_audio_video_gallery value="no"/>
    <is_mathjax value="no"/>
    <has_embed value="no"/>
    <!--These two below seem to contradict each other -->
    <exclude_from_nav value="no">
    <list_in_nav value="no"/> 
    <content>
    <![CDATA[
        <h2 class="subhead">Course Meeting Times</h2>
        <p>Lectures: 2 sessions / week, 1.5 hours / session</p>
        <h2 class="subhead">Prerequisites</h2>
        <p>There are no prerequisites for this course.</p>
        <h2 class="subhead">Description</h2>
        <p>We will devote our time this term to three activities. First, we will study the history of the book in Europe from Gutenberg (ca. 1450) to the French Revolution (ca. 1800). Second, we will examine in detail books and prints in the collections of the MIT Libraries and the MIT Museum made from 1450 to 1800 in Europe. Third, we will build a functioning, durable printing press based on Early Modern European designs. These three activities are designed to provide a holistic view of print and its impact from roughly 1450 to 1800. Along the way, we will also pause periodically to consider the parallels between the world of print in the Early Modern period and the rapidly changing media landscape today. Was there a &quot;printing revolution&quot; in the Renaissance? Are we living through another media revolution today?</p>
        <h2 class="subhead">Grading Policy</h2>
        <div class="maintabletemplate">
        <table summary="See table caption for summary." class="tablewidth75">
            <caption class="invisible">Grading criteria.</caption>  <!-- BEGIN TABLE HEADER (for MIT OCW Table Template 2.51) -->
            <thead>
                <tr>
                    <th scope="col">ACTIVITIES</th>
                    <th scope="col">PERCENTAGES</th>
                </tr>
            </thead>
            <!-- END TABLE HEADER -->
            <tbody>
                <tr class="row">
                    <td>Attendance and active participation in all class meetings</td>
                    <td>20%</td>
                </tr>
                <tr class="alt-row">
                    <td>Four forum postings reflecting comparatively on books in the Early Modern period and today</td>
                    <td>20%</td>
                </tr>
                <tr class="row">
                    <td>
                    <p>Two five-page papers</p>
                    <p><em>Topics may include the political, cultural, and economic effects of printing in the Early Modern world; detailed bibliographical and content analysis of holdings in MIT's <a href="http://libraries.mit.edu/archives/research/rare-books.html">Rare Books Collection</a>; reports on progress made towards building a printing press.</em></p>
                    </td>
                    <td>30%</td>
                </tr>
                <tr class="alt-row">
                    <td>Class project to design and build a handset printing press, with appropriate guidance from the Director of the MIT Hobby Shop</td>
                    <td>30%</td>
                    <!-- FOUR ROWS -->
                </tr>
            </tbody>
        </table>
        </div>
        <h2 class="subhead">Required Readings</h2>
        <p><a href="http://www.amazon.com/exec/obidos/ASIN/0866981489/ref=nosim/mitopencourse-20"><img border="0" align="absmiddle" alt="Buy at Amazon" src="/images/a_logo_17.gif" /></a> Del Col, Andrea, ed. <em>Domenico Scandella Known as Menocchio: His Trials Before the Inquisition (1583&ndash;1599)</em>. Translated by John and Anne C. Tedeschi. Mrts, 1996. ISBN: 9780866981484.</p>
        <p><a href="http://www.amazon.com/exec/obidos/ASIN/1421409887/ref=nosim/mitopencourse-20"><img border="0" align="absmiddle" alt="Buy at Amazon" src="/images/a_logo_17.gif" /></a> Ginzburg, Carlo. <em>The Cheese and the Worms: The Cosmos of a Sixteenth-Century Miller</em>. Translated by John and Anne C. Tedeschi. John Hopkins University Press, 2013. ISBN: 9781421409887. [Preview with <a href="http://books.google.com/books?id=NZzyAAAAQBAJ&amp;pg=PAfrontcover">Google Books</a>]</p>
        <p>For more on the history of print, you may wish to consult the <a href="http://www.historyofinformation.com/index.php">History of Information</a> web site, or the following books:</p>
        <p><a href="http://www.amazon.com/exec/obidos/ASIN/8170462665/ref=nosim/mitopencourse-20"><img border="0" align="absmiddle" alt="Buy at Amazon" src="/images/a_logo_17.gif" /></a> Febvre, Lucien, and Henri-Jean Martin. <em>The Coming of the Book: The Impact of Printing, 1450&ndash;1800</em>. Translated by David Gerard. Seagull Books, 1976. ISBN: 9788170462668. [Preview with <a href="http://books.google.com/books?id=9opxcMjv4TUC&amp;pg=PAfrontcover">Google Books</a>]</p>
        <p><a href="http://www.amazon.com/exec/obidos/ASIN/0300178212/ref=nosim/mitopencourse-20"><img border="0" align="absmiddle" alt="Buy at Amazon" src="/images/a_logo_17.gif" /></a> Pettegree, Andrew. <em>The Book in the Renaissance</em>. Yale University Press, 2011. ISBN: 9780300178210.</p>
        <p>All other readings can be found in the table in the <a href="./resolveuid/2bdc85dbfadb365bf11d42f687b509c1">Readings and Videos</a> section.</p>
        <h2 class="subhead">Printing Press Construction</h2>
        <p>Our goal is to complete construction of our printing press by the end of Week 11, leaving us three weeks to learn how to be handset printers.</p>
    ]]>
    </content>
    <bottom_html>
        <![CDATA[
        ]]>
    </bottom_html>
</section>